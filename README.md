# readout
A tool for inserting markdown content into your site as HTML

This project is extremely early in its development. The premise is when it comes to writing actual content, you probably don't need anything besides markdown. As a huge fan of it, I'd like to be able to write all of my word-stuff in good ol' markdown.

What you see so far is just a very rough prototype, but it gets the job done. More to come.

## Installation

Prerequisites:
* bower

  ```bash
  git clone https://github.com/asimpletune/readout.git  # clone repo
  cd readout && bower install                           # install dependencies
  ```

## How to use

1. Put dependencies into your html or template

  ```html
  <script type="text/javascript" src="/javascripts/showdown.min.js"></script>
  <script type="text/javascript" src="/javascripts/jquery.min.js"></script>
  <script type="text/javascript" src="/javascripts/readout.js"></script>
  ```

2. Add the *data-readout-src* tag where you want the markdown appended to in your HTML, with the value being the path to the markdown file, as shown below.

  ```html
  <div data-readout-src="/content/hello.md"></div>
  ```

  You can also nest your markdown, to match your file-system's hierarchy. For example:
  ```bash
  # Directory of resources
  content/
  ├── example.md
  └── hello.md
  ```

  ```html
  <div data-readout-src="content">
    <div data-readout-src="example.md"></div>
    <div data-readout-src="hello.md"></div>
  </div>
  ```

3. Call readout!
  ```html
  <script type="text/javascript">
    $(function() {
      Readout();
    });
  </script>
  ```
  You can also specify your own custom attribute, but it must follow the string pattern *data-\**

  ```html
  <script type="text/javascript">
    $(function() {
      Readout("data-bobs-app-location");
    });
  </script>
  ```

** Here's a [gh-pages example](http://asimpletune.github.io/readout/ "gh-pages example"), or an  [express example](https://github.com/asimpletune/readout/tree/master/example) project that demonstrates the idea **

## More to come

1. Add an absolute src tag to override the file hierarchy.
2. npm and bower packaging
3. Multicolumn layout support
