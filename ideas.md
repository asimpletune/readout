# Ideas

1. Batch GET requests
2. Nest resources (pertaints to batching)

  example:
  ```bash
  # Directory of resources
  content/
  ├── example.md
  └── hello.md
  ```

  ```html
  <div data-readout-src="/content">
    <div data-readout-src="/hello.md">
    </div>    
  </div>
  ```
