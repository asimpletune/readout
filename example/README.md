# Readout example

This is a very basic example. I use it as I'm developing.

## Installation

Prerequisites:
* Node.js
* npm

```bash
npm install
npm start # server should be running at localhost:3000
```
